
/**
 * Module dependencies.
 */

var express = require('express'),
    app = express();

// all environments
app.set('view engine', 'html');
app.use(express.bodyParser());

app.get('/', function(req, res){
    res.sendfile('./views/index.html');
});

app.configure(function() {
    app.use(express.static(__dirname));
});

app.listen(3000);
