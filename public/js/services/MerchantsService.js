MedievalMarketplaceApp.factory('Merchants', function(){
    var merchants;
    
    return merchants = [
        {
            id: 1,
            name: 'Mr. Asham',
            goods: [
                {
                    name: 'Apple',
                    type: 'nutrition',
                    kind: 'food',
                    boostHP: 10,
                    boostSP: 0,
                    qty: 1
                },
                {
                    name: 'Elixir',
                    type: 'nutrition',
                    kind: 'potion',
                    boostHP: 200,
                    boostSP: 100,
                    qty: 1
                },
                {
                    name: 'Panacea',
                    type: 'nutrition',
                    kind: 'potion',
                    boostHP: 100,
                    boostSP: 20,
                    qty: 1
                },
                {
                    name: 'Ordinary Manteau',
                    type: 'equipment',
                    kind: 'armor',
                    property: 'normal',
                    level: '0',
                    health: '0.92'
                }
            ],
            shopName: 'Mr. Ashams Bounty Store',
            shopOpen: false
        },
        {
            id: 2,
            name: 'Lili',
            goods: [
                {
                    name: 'Green Herb',
                    type: 'nutrition',
                    kind: 'food',
                    boostHP: 20,
                    boostSP: 5,
                    qty: 1
                },
                {
                    name: 'Opal',
                    type: 'mineral',
                    kind: 'gem',
                    property: 'light',
                    qty: 1
                },
                {
                    name: 'Iron',
                    type: 'mineral',
                    kind: 'metal',
                    property: 1
                },
                {
                    name: 'Alloy',
                    type: 'mineral',
                    kind: 'metal',
                    property: 3
                }
            ],
            shopName: 'Lilis Treasure Trove',
            shopOpen: false
        },
        {
            id: 3,
            name: 'Barbara',
            goods: [
                {
                    name: 'Sapphire',
                    type: 'mineral',
                    kind: 'gem',
                    property: 'Water',
                    qty: 1
                },
                {
                    name: 'Garnet',
                    type: 'mineral',
                    kind: 'gem',
                    property: 'Fire',
                    qty: 1
                },
                {
                    name: 'Moonstone',
                    type: 'mineral',
                    kind: 'gem',
                    property: 'Dark',
                    qty: 1
                }
            ],
            shopName: 'Jewelers Delight',
            shopOpen: false
        }
    ];
});