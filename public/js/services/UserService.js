MedievalMarketplaceApp.factory('User', function(){
    var user;

    return user =
    {
        name: 'Earl of Corning Hall',
        hp: {
            current: 252,
            total: 328
        },
        sp: {
            current: 12,
            total: 85
        },
        cash: 7568,
        bag: [
            {
                name: 'Blueberry',
                type: 'nutrition',
                kind: 'food',
                boostHP: 15,
                boostSP: 10,
                qty: 5
            },
            {
                name: 'Apple',
                type: 'nutrition',
                kind: 'food',
                boostHP: 10,
                boostSP: 0,
                qty: 12
            },
            {
                name: 'Egg',
                type: 'nutrition',
                kind: 'food',
                boostHP: 15,
                boostSP: 3,
                qty: 3
            },
            {
                name: 'Milk',
                type: 'nutrition',
                kind: 'food',
                boostHP: 20,
                boostSP: 10,
                qty: 7
            },
            {
                name: 'Dragonhide Manteau',
                type: 'equipment',
                kind: 'armor',
                property: 'Fire',
                level: 2,
                health: 0.8
            },
            {
                name: 'Fisherman\'s Blade',
                type: 'equipment',
                kind: 'weapon',
                property: 'Water',
                level: 0,
                health: 0.3
            },
            {
                name: 'Opal',
                type: 'mineral',
                kind: 'gem',
                property: 'light',
                qty: '2'
            },
            {
                name: 'Alloy',
                type: 'mineral',
                kind: 'metal',
                property: 3,
                qty: '5'
            }
        ]
    };
});