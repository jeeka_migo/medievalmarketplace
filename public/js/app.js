/**
 *
 * Created by jeanella on 12/5/13.
 *
 */

var MedievalMarketplaceApp = angular.module("MedievalMarketplaceApp", []);

MedievalMarketplaceApp.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider
            .when('/merchants', {
                templateUrl: '/views/merchants.html',
                controller: 'MerchantsController'
            })
            .when('/blacksmith', {
                templateUrl: '/views/blacksmith.html',
                controller: 'BlacksmithController'
            })
            .when('/apothecary', {
                templateUrl: '/views/apothecary.html',
                controller: 'ApothecaryController'
            })
            .when('/user', {
                templateUrl: '/views/user.html',
                controller: 'UserController'
            })
            .otherwise({
                redirectTo: '/'
            });
    }]);