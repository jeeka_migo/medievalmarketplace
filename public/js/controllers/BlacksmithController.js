MedievalMarketplaceApp.controller('BlacksmithController', function($scope){
	$scope.message = "Blacksmith";
	$scope.maxEquipmentLevel = 10;
    $scope.maxEquipmentHealth = 1.0;

	$scope.blacksmith = {
		upgradeActive: false,
		repairActive: false,
		repairFee: 50
	}

	$scope.metals = [
		{
			name: 'Iron',
			property: 1
		},
		{
			name: 'Tin',
			property: 2
		},
		{
			name: 'Alloy',
			property: 3
		},
        {
            name: 'Alloy',
            property: 3
        },
        {
            name: 'Alloy',
            property: 3
        },
        {
            name: 'Tin',
            property: 2
        }
	];

	$scope.forge = {
		metalQtyMax: 3,
		metals: [],
		equipment: {}
	};

	$scope.chooseMetal = function(index){
		if($scope.forge.metals.length < $scope.forge.metalQtyMax)
		{
			$scope.forge.metals.push($scope.metals[index]);
		}
        $scope.metals.splice(index,1);
	}

	$scope.upgradeEquipment = function(index){
        if($scope.user.bag[index].level < $scope.maxEquipmentLevel)
        {
            $scope.blacksmith.upgradeActive = true;
            $scope.forge.equipment = $scope.user.bag[index];
            $scope.user.bag.splice(index,1);
        }
	}

	$scope.repairEquipment = function(equipment){
		if(equipment.health < 1.0){
			equipment.health += 0.2;
			$scope.user.cash -= $scope.blacksmith.repairFee;
		}
		if(equipment.health > 1.0){
			equipment.health = 1.0;
		}
	}

	$scope.forgeUpgrade = function(){
		for(i=0; i< $scope.forge.metals.length; i++){
            if($scope.forge.equipment.level < $scope.maxEquipmentLevel)
            {    switch($scope.forge.metals[i].property){
                    case 1:	$scope.forge.equipment.level += 0.6;
                            break;
                    case 2:	$scope.forge.equipment.level += 1;
                            break;
                    case 3: $scope.forge.equipment.level += 1.2;
                            break;
                    default: $scope.forge.equipment.level += 0;
                }
                $scope.forge.metals.splice(i,1);
            }
            else {
				$scope.forge.equipment.level = $scope.maxEquipmentLevel;
				break;
			}
		}

        for(var j=0 ; j < $scope.forge.metals.length; j++){
            $scope.metals.push($scope.forge.metals[j]);
            $scope.forge.metals.splice(j,1);
        }

		$scope.user.bag.push($scope.forge.equipment);
		$scope.forge.equipment = {};
        $scope.blacksmith.upgradeActive = false;
	}
});