MedievalMarketplaceApp.controller('UserController', function($scope){
	$scope.message = "My Gear";

	$scope.consume = function(item){
		if(item.qty > 0){
			$scope.user.hp.current += item.boostHP;
			$scope.user.sp.current += item.boostSP;
			item.qty--;

			$scope.checkIfCapped($scope.user.hp);
			$scope.checkIfCapped($scope.user.sp);
		}
        else {
            $scope.user.bag.splice($scope.user.bag.indexOf(item),1);
        }
	}

	$scope.drop = function(bagItem, qty){
		bagItem.qty -= qty;

		if(qty > bagItem.qty){
			bagItem.qty = 0;
		}
	}

	$scope.checkIfCapped = function(value){
		if(value.current > value.total){
			value.current = value.total;
		}
	}
});