MedievalMarketplaceApp.controller('MerchantsController', function($scope, Merchants){
	$scope.message = "Merchants";

    $scope.merchants = Merchants;

    $scope.toggleMerchantShop = function(merchant) {
        if(merchant.shopOpen){
            merchant.shopOpen = false;
        }
        else {
            merchant.shopOpen = true;
        }
    }

    $scope.basket = [];

    $scope.addToBasket = function(item) {
        $scope.basket.push(item);
    }

    $scope.buyAll = function(){
        for(var i=0; i < $scope.basket.length; i++){
            $scope.user.bag.push($scope.basket[i]);
        }

        $scope.emptyBasket();
    }

    $scope.drop = function(index){
        $scope.user.bag.splice(index,1);
    }

    $scope.emptyBasket = function(){
        $scope.basket = {};
    }
});