/**
 *
 * Created by jeanella on 12/5/13.
 *
 */

MedievalMarketplaceApp.controller("MainController", function($scope, User) {
    $scope.setActive = function(index) {
        for(var i=0; i < $scope.marketplace.length; i++)
        {
            $scope.marketplace[i].isActive = false;
        }
        $scope.marketplace[index].isActive = true;
    }

    $scope.bagURL = "/views/partials/bag.html";
    $scope.goodsURL = "/views/partials/goods.html";
    $scope.shopURL = "/views/partials/shop.html";
    $scope.basketURL = "/views/partials/basket.html";
    $scope.navbarURL = "/views/navbar.html";

    $scope.merchantsURL = "/views/merchants.html";
    $scope.blacksmithURL = "/views/blacksmith.html";
    $scope.apothecaryURL = "/views/apothecary.html";
    $scope.userURL = "/views/user.html";

    $scope.user = User;
});
