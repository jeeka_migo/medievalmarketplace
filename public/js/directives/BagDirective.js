MedievalMarketplaceApp.directive('bag', function(){

    return {
        restrict: 'E',
        templateUrl: '/views/partials/bag.html',
        scope: {
            pockets: '='
        },
        controller: function($scope, User){
            $scope.items = User.bag;
        }
    }
});